PyOracle improvisation + i-score
================================

Integration of the Max PyOracle_ interface with the intermedia sequence
i-score_.

.. _PyOracle: https://pypi.python.org/pypi/PyOracle
.. _i-score: http://i-score.org
